# \<podcast-inditex\>



## Install the Polymer-CLI

First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. Then run `$ polymer install ` and `polymer serve` to serve your application locally.

## Viewing Your Application

```
$ polymer serve
```

## Building Your Application

First, let's make sure that we have all the dependencies installed:

```
$ polymer install 
```

```
$ polymer build
```

This will create builds of your application in the `build/` directory, optimized to be served in production. You can then serve the built versions by giving `polymer serve` a folder to serve from:

```
$ polymer serve build/dev
```

```
$ polymer serve build/prod
```

The `dev` build is not vulcanized nor minimized. The `prod` build is both minimized and vulcanized.

# Known Issues

## Some responses as text/html
When getting the information from the feeds webpage through CORS, some endpoints ignore my Headers that explicitly request for "Content-type": "application/xml" and "accept" : "application/xml" and they send "text/html" so I can't display properly the data. 

I tried everything I know, setting the headers, different response types, using jQuery and datatype="xml", pure javascript with XMLHttpRequest, but to no avail. What left me speechless is that through Postman or HTTPie I'm getting the correct RSS feed even without setting the headers, just doing the simple request I get the data I expect.

## Caching mechanism
For caching I use `localStorage` that in Chrome has a limitation of 5MB. I've tried to improve it as much as I could removing unnecessary information to save space. A better solution would be to use IndexedDB or some higher level library like [Dexie.js](http://dexie.org/), [LocalForage](https://localforage.github.io/localForage/#localforage).

As an improvement, I would use `localForage` because the API is really similar and straightforward like localStorage